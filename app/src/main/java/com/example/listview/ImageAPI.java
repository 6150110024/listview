package com.example.listview;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;



public interface ImageAPI {

    public static final String BASE_URL = "http://192.168.43.86/";
    @GET("/retrofit/list_images.php")
    Call<List<ImagesList>> getImages();
}
